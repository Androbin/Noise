package de.androbin.noise;

public interface Noise1D {
  float noise( float x );
}